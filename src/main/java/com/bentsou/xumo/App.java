package com.bentsou.xumo;

import com.bentsou.xumo.data.Banner;
import com.bentsou.xumo.data.Campaign;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class App {
    private static final DateTimeFormatter DATE_PATTERN = DateTimeFormatter.ofPattern("d-MMM-yyyy");


    public static void main( String[] args ) {
        App app = new App();
        app.execute();
    }

    private void execute() {
        ArrayList<Campaign> campaigns = readActiveCampaigns();
        Map<Integer, Banner> bannerMap = readBanners();

        generateRandomIndices(campaigns.size(), 10).stream()
                .map(index -> campaigns.get(index))
                .map(campaign -> bannerMap.get(campaign.getCid()))
                .forEach(System.out::println);
    }

    private Map<Integer, Banner> readBanners() {
        try {
            Path path = Paths.get(getClassLoader().getResource("banners.csv").toURI());
            return Files.lines(path)
                        .skip(1)
                        .map(this::lineToBanner)
                        .collect(Collectors.toMap(banner -> banner.getCid(), Function.identity()));
        } catch (URISyntaxException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Banner lineToBanner(String line) {
        String[] components = line.split(",");

        try {
            return new Banner(Integer.parseInt(components[1]), components[2]);
        } catch (NumberFormatException e) {
            throw new RuntimeException(e);
        }
    }

    private ArrayList<Campaign> readActiveCampaigns() {
        try {
            Path path = Paths.get(getClassLoader().getResource("campaign.csv").toURI());
            return Files.lines(path)
                        .skip(1)
                        .map(this::lineToCampaign)
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .collect(Collectors.toCollection(ArrayList::new));
        } catch (URISyntaxException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Optional<Campaign> lineToCampaign(String line) {
        String[] components = line.split(",");

        LocalDate start = LocalDate.parse(components[2], DATE_PATTERN);
        LocalDate end = LocalDate.parse(components[3], DATE_PATTERN);
        LocalDate now = LocalDate.now();

        if ((start.isBefore(now) || start.isEqual(now)) &&
            (end.isAfter(now) || end.isEqual(now))) {
            int cid = Integer.parseInt(components[0]);
            return Optional.of(new Campaign(cid, true));
        } else {
            return Optional.empty();
        }

    }

    private Set<Integer> generateRandomIndices(int maxIndex, int count) {
        if (count >= maxIndex + 1) {
            throw new RuntimeException("Not enough indices");
        }

        Random rand = new Random();
        Set<Integer> indices = new HashSet<>();

        while (indices.size() != count) {
            indices.add(rand.nextInt(maxIndex));
        }

        return indices;
    }

    private ClassLoader getClassLoader() {
        return getClass().getClassLoader();
    }
}
