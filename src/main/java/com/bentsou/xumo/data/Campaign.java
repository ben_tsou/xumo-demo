package com.bentsou.xumo.data;

public class Campaign {
    private int cid;
    private boolean active;

    public Campaign(int cid, boolean active) {
        this.cid = cid;
        this.active = active;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
