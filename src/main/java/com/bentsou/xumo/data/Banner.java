package com.bentsou.xumo.data;

public class Banner {
    private int cid;
    private String imageUrl;

    public Banner(int cid, String imageUrl) {
        this.cid = cid;
        this.imageUrl = imageUrl;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return imageUrl;
    }
}
